#coding latin-1
import nltk
from nltk import Nonterminal, nonterminals, Production, CFG, grammar, parse, RecursiveDescentParser, word_tokenize
import nlpnet, json, sys, pprint
from nltk.tree import *
from nltk.draw import tree

#nlpnet.set_data_dir('/home/jean/Programacao/tcc/src/mining/projects/ghost-core/src/semantic/data/pos-pt')
nlpnet.set_data_dir('/home/jean/Programacao/tcc/src/mining/projects/ghost-core/src/semantic/data/srl-pt')
#tagger = nlpnet.POSTagger()
#print tagger.tag('O rato roeu a roupa do rei de Roma.')
#[[(u'O', u'ART'), (u'rato', u'N'), (u'roeu', u'V'), (u'a', u'ART'), (u'roupa', u'N'), (u'do', u'PREP+ART'), (u'rei', u'N'), (u'de', u'PREP'), (u'Roma', u'NPROP'), (u'.', 'PU')]]

reload(sys)
sys.setdefaultencoding('Cp1252')

tagger = nlpnet.SRLTagger()
frazes = [
    'meu irmão não quer comprar uma TV LED',
    'quero comprar uma TV Sansung, alguém tem alguma para me vender?',
    'o meu irmão quer uma TV LED',
    'meu irmão Mateus, está procurando uma TV de LED, alguém tem?',
    'quem tem uma TV LED pra me vender?',
    'quero uma TV de preferência samsung e LED, alguém?',
    'sera que é bom?',
    'sera que o novo moto g é tão bom assim?',
    'eim Mateus, o que tu acha desta?',
    'vendo TV LED samsung, R$1000',
    'quero uma TV nova, mas tem que ser da samsung, e de LED, de preferencia tela grande, maior que 40 polegadas.',
    'o eduardo e a monica querem comprar uma casa, alguém ai tem uma pra vender pra eles?',
    'quero comprar uma TV de marca boa',
    'quero comprar um note, alguém tem alguma para me vender?',
    'meu irmão quer comprar um note dell',
    'o meu irmão quer um note',
    'meu irmão Fulano, esta procurando um ultrabook, alguém tem?',
    'quem tem um ultrabook pra me vender?',
    'eu amo esse novo ultrabook, quero um :D',
    'quero um notebook de preferencia lenovo e preço até 2 mil reais',
    'quero um notebook novo, mas tem que ser da lenovo, e de 500 gb, de preferencia tela grande, maior que 15 polegadas.',
    'o Fulano e a Fulana querem comprar um notebook, alguém ai tem uma pra vender pra eles?',
    'quero um notebook de marca boa, alguém tem para me vender?',
    'vendo notebook, R$1000 Dell Inspiron 15',
    'eu quero um notebook nas cores vermelha e azul', 
    'eu não quero um notebook, quero uma tv'
]

gNEG=("""
NEG -> DEF | CONJ
CONJ -> 'mas' DEF
DEF -> 'não' | 'nunca' | 'exceto'
""", ['não', 'nunca', 'exceto'])

gDSJ=("""
DSJ -> DEF | ACT_C | ACT_V | DEF ACT_C | DEF ACT_V
DEF -> 'quero' | 'queria' | 'quis' | 'amo' | 'desejo' | 'preciso' | 'quer' | 'procura' | 'procuro' | 'tenho' 'que' ACT_C | 'tenho' 'que' ACT_V
ACT_C -> 'comprar' | 'compro' | 'ter' | 'ganhar'
ACT_V -> 'vender' | 'vendo' | 'desapego'
""", ['quero','queria','amo','desejo','preciso','comprar','compro','ter','ganhar','vender','vendo','desapego','quer', 'procura', 'procuro'])

gIND=("""
IND -> DEF | REF | REF CONJ REF | DEF CONJ REF | REF CONJ DEF
DEF -> 'eu' | 'alguém'
CONJ -> 'e' | ','
REF -> 'meu' PAR | 'minha' PAR | 'teu' PAR | 'tua' PAR
PAR -> 'irmão' | 'irmã' | 'mãe' | 'pai' | 'tiu' | 'tia' | 'primo' | 'prima'
""", ['eu','e',',','meu','minha','teu','tua','irmão','irmã','mãe','pai', 'tiu', 'tia', 'primo', 'prima'])

gOBJ=("""
OBJ  -> DEF | MC | DEF MC | MC DEF 
DEF  -> 'notebook' | 'tv' | OBJ
MC -> 'samsung' | 'lg' | 'motorola' | OBJ | DEF
""", ['notebook', 'tv', 'samsung', 'lg', 'motorola'])

gCOR=("""
COR -> DEF | DEF | TOM | REF | TOM 
DEF -> 'azul' | 'azulado' | 'azulada' | 'branca' | 'branco' | 'brancas' | 'brancos' | 'vermelha' | 'vermelho' 
TOM -> 'claro' | 'escuro' 
REF -> 'cor' | 'cores' | 'cor' | DEF | TOM | 'na' | 'nas' | REF | 'da' | 'das' | REF | 'em' | DEF | 'em' | DEF | TOM 
""", ['azul' , 'azulado' , 'azulada' , 'branca' , 'branco' , 'brancas' , 'brancos' , 'vermelha' , 'vermelho'  ])

gCOR2=("""
COR  -> DEF | REF
REF -> 'cor' | 'cor' DEF | 'na' REF | 'da' REF | 'em'
DEF -> 'azul' | 'branca' | 'branco' | 'vermelha' | 'vermelho'
""", ['cor', 'na', 'da', 'em', 'azul', 'branca', 'branco', 'vermelho', 'vermelha'])


lGram=[gOBJ, gCOR, gIND, gDSJ, gNEG]

def valGrammar(grammars, sent):
  gFound = []
  for g,words in grammars:
    words2 = [x.decode('utf8') for x in words]
    words = words2
    grammar = CFG.fromstring(g.decode('utf8'));
    parser = nltk.ChartParser(grammar)
    tokens = [t for t in nltk.word_tokenize(sent) if t in words]
    try:
      for tree in parser.parse(tokens):
        if len(tree) > 0:
          #tree.draw()
          gFound.append(tree)
          break
    except:
      pass
  return gFound

pp = pprint.PrettyPrinter(depth=6)

cont = 0
size = 0
for f in frazes:
  print "INICIO================================================"
  print "Sentença:", f
  sents = tagger.tag(f)
  gFound=[]
  size+=len(sents)
  for sent in sents:
    if len(sent.arg_structures)>0:
      enc=False
      print json.dumps( sent.arg_structures, ensure_ascii=False, sort_keys=True, indent=4, separators=(',', ': ') )
      for vb, st in sent.arg_structures:
        #print "VERBO",vb
        if type(st) == dict:
          for k,v in st.items():
            print "Agrupamento:",k,v
            z=valGrammar(lGram, ' '.join(v))
            if len(z)>0:
              enc=True
              gFound.append(z)
      if enc:
        cont+=1
    else:
      print "Complexo:",f
      z=valGrammar(lGram, f.decode('utf8'))
      if len(z)>0:
        cont+=1
        gFound.append(z)      
    pp.pprint(gFound)
  print "FIM==================================================="
print "\nDeteccoes "+str(cont)+"/"+str(size)
