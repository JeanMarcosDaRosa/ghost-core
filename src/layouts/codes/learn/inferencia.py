#inferencia.py

def getAllProducts():
    db = MongoClient().ghost
    products=[]
    for p in db['collect_product'].find({}):
        products.append(p)
    return products

#TODO problema se ouver uma conexao com um objeto que ja eh a conexao de um objeto, pois nao eh recursivo, e soh funcionao se tiver na ordem correta (deve ser corrigido)
#TODO corrigir quando ouver duas arvores para a mesma gramatica, por exemplo pro OBJ, onde se emcaixa em titulo e categoria, como o for eh para a lista de conexoes, quando ele encontra a primeira arvore, ja para a iteracao e vai para o proximo, deve pegar a segunda arvore tambem e fazer um merge entre as duas.
def makeTree(arvores, lista, obj):
    root = Tree('ROOT', [])
    resultTrees = []
    target=None
    for con in lista:
        x, y = None, None
        for t in arvores:
            if type(t) == nltk.Tree:
                if t.label() == con[0]:
                    x = t
                    if not len(con[1]) > 0:
                        break
                    continue
                if t.label() in con[1]:
                    y = t
                    if y.label()==obj:
                        target=y
                if x!=None and y!=None:
                    break
        if x!=None and y!=None:
            x.append(y)
            arvores.remove(y)
        if x!=None:
            if x.label()==obj:
                target=x
            resultTrees.append(x)
    for t in resultTrees:
        if type(t) == nltk.Tree:
            root.append(t)
    return root, target

def getAllLabels(tree):
    ret=[]
    if tree!=None:
        if type(tree) is nltk.Tree:
            ret.append(tree.label())
            for t in tree:
                if type(t) is nltk.Tree:
                    #ret.append(t.label())
                    r = getAllLabels(t)
                    for x in r:
                        ret.append(x)

    return ret

def getNodes(parent, listaBD):
    li=[]
    for node in parent:
        if type(node) is nltk.Tree:
            if node.label() in listaBD:
                li.append((node.label(), node.leaves(), listaBD[node.label()]))
            l = getNodes(node, listaBD)
            if len(l) > 0:
                li.append(l)
    return li

def fzmatch(str1, str2):
    if str1 == str2: return True
    r=fuzz.token_set_ratio(str1, str2)
    x=(len(str1.lower()) > 6 and r > 85)
    return x


#inferir o sujeito, e a intencao
def findInference(products, listaBD, listaMC, target):
    attrs = []
    for x in getNodes(target, listaBD):
        z=x
        while type(z) == type([]):
            z=z[0]
        attrs.append(z)

    grupos = []

    for r, vs, attr in attrs:
        rp = []
        for prod in products:
            for at in attr:
                ats = at.split('>')
                if len(ats) > 1:
                    r = prod
                    for a in ats:
                        if a in r:
                            r = r[a]
                    for v in vs:
                        if r != None and (type(r) == type('') or type(r) == unicode):
                            if fzmatch(v.lower(), r.lower()): #match com fuzzy
                                rp.insert(0, prod['_id'])
                else:
                    if at in prod:
                        for v in vs:
                            if fzmatch(v.lower(), prod[at].lower()):
                                rp.insert(0, prod['_id'])
        if len(rp) > 0:
            grupos.append(rp)

    #intersection
    result=[]
    for g in grupos:
        if len(result) == 0:
            for x in g:
                result.append(x)
        else:
            ist = set(result).intersection(g)
            if len(ist) > 0:
                for x in ist:
                    result.append(x)
            else:
                break

    return result

def exprValida(expr_minima, arvores):
    labels=[]
    for a in arvores:
        r = getAllLabels(a)
        for l in r:
            labels.append(l)

    for val in labels:
        exec(val+'=True')

    result = False
    try:
        allvars = re.findall(r'[A-Z]+[A-Z]*', expr_minima)
        for v in allvars:
            if v not in labels:
                exec(val+'=False')
        result = eval(expr_minima)
    except:
        pass
    for val in labels:
        exec('del '+val)
    return result


def run(dispach, logger, procName, objeto, expr_minima, retorno=None):
    logger.info("EXEC ["+procName+"] retorno: "+str(retorno))
    try:
        if retorno!=None and 'arvore_sintatica' in retorno:
            if not 'ontologia' in retorno:
                logger.warn("Nao encontrado layout da ontologia para realizar inferencia")
            else:
                listaCX = []
                listaBD = {}
                listaMC = []

                arvores = [ t for t in retorno['arvore_sintatica'] ]

                if exprValida(expr_minima, arvores):
                    for x in retorno['ontologia']:
                        if len(x[1]['CX'])>0:
                            for cx in x[1]['CX']:
                                if type(cx) == dict:
                                    for a, b in cx.items():
                                        listaCX.append([x[0], cx[a]])
                                else:
                                    listaCX.append([x[0], cx])
                        else:
                            listaCX.append([x[0], [] ])


                    root, objeto = makeTree(arvores, listaCX, objeto)
                    #objeto.draw()
                    #root.draw()
                    result = []
                    if objeto != None:

                        products = getAllProducts()

                        for x in retorno['ontologia']:
                            if len(x[1]['BD']) > 0:
                                for bd in x[1]['BD']:
                                    if type(bd) == dict:
                                        for a, b in bd.items():
                                            listaBD[a]=b


                        for x in retorno['ontologia']:
                            if len(x[1]['MC']) > 0:
                                for mc in x[1]['MC']:
                                    if type(mc) == dict:
                                        for a, b in mc.items():
                                            listaMC.append([a, b[0]])

                        #print target
                        #print listaBD

                        result = findInference(products, listaBD, listaMC, objeto)

                    if len(result) > 0:
                        logger.info("Produto selecionado para o usuario: "+result[0])
                        dispach(procName, {"post": retorno['post'], "produto": result[0]}, True)
                    else:
                        logger.warn("Nenhum produto correspondeu as especificacoes do usuario")
                        dispach(procName, False, True)
                else:
                    logger.warn("Expressao minima nao atendida: "+str(expr_minima))
        else:
            logger.info("Sem arvore sintatica para realizar inferencia")
        dispach(procName, False, True)
    except Exception, e:
        logger.error(e.message)
        dispach(procName, False, True)


try:
    db = MongoClient().ghost
except:
    import nltk
    import logging
    from nltk.tree import *
    from nltk.draw import *
    from pymongo import MongoClient
    from bson.objectid import ObjectId
    from fuzzywuzzy import fuzz

    def dispach(procName, a, b):
        return procName, a, b

    logger = logging.getLogger("inferencia.py")
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    logger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    #ret = {u'arvore_sintatica': [Tree('INT', [Tree('DSJ', ['quero'])])], u'ontologia': [(u'IND', {'BD': [], 'CX': [], 'MC': []}), (u'OBJ', {'BD': [{u'TITULO': [u'titulo', u'definicoes>Modelo']}, {u'CAT': [u'categorias', u'titulo']}, {u'MARCA': [u'definicoes>Marca', u'titulo']}], 'CX': [{u'OBJ': [u'COR']}], 'MC': [{u'TITULO': [u'filter_padrao.py']}, {u'CAT': [u'filter_padrao.py']}, {u'MARCA': [u'filter_padrao.py']}]}), (u'COR', {'BD': [], 'CX': [], 'MC': []}), (u'INT', {'BD': [], 'CX': [], 'MC': []})], u'post': '553eed5a4d52d13061e105a7'}
    ret = {u'post': '',u'arvore_sintatica': [Tree('OBJ', [Tree('REF', ['um']), Tree('CAT', ['notebook'])]), Tree('OBJ', [Tree('REF', ['um']), Tree('TITULO', ['notebook'])]), Tree('IND', [Tree('DEF', ['eu'])]), Tree('COR', [Tree('REF', ['na', Tree('REF', ['cor', Tree('DEF', ['vermelha'])])])]), Tree('INT', [Tree('DSJ', ['quero'])])], u'ontologia': [(u'IND', {'BD': [], 'CX': [], 'MC': []}), (u'OBJ', {'BD': [{u'TITULO': [u'titulo', u'definicoes>Modelo']}, {u'CAT': [u'categorias', u'titulo']}, {u'MARCA': [u'definicoes>Marca', u'titulo']}], 'CX': [{u'OBJ': [u'COR']}], 'MC': [{u'TITULO': [u'filter_padrao.py']}, {u'CAT': [u'filter_padrao.py']}, {u'MARCA': [u'filter_padrao.py']}]}), (u'COR', {'BD': [{u'DEF': [u'definicoes>Cor']}], 'CX': [], 'MC': []}), (u'INT', {'BD': [], 'CX': [], 'MC': []})]}
    #ret = {u'arvore_sintatica': [Tree('INT', [Tree('DSJ', ['quero'])])], u'ontologia': [(u'IND', {'BD': [], 'CX': [], 'MC': []}), (u'OBJ', {'BD': [{u'TITULO': [u'titulo', u'definicoes>Modelo']}, {u'CAT': [u'categorias', u'titulo']}, {u'MARCA': [u'definicoes>Marca', u'titulo']}], 'CX': [{u'OBJ': [u'COR']}], 'MC': [{u'TITULO': [u'filter_padrao.py']}, {u'CAT': [u'filter_padrao.py']}, {u'MARCA': [u'filter_padrao.py']}]}), (u'COR', {'BD': [], 'CX': [], 'MC': []}), (u'INT', {'BD': [], 'CX': [], 'MC': []})], u'post': '553eed5a4d52d13061e105a7'}
    run(dispach, logger, "inferencia", "OBJ", "(INT & OBJ)", retorno=ret)
