# -*- coding: utf8 -*-

import string
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from fuzzywuzzy import utils
from nltk import metrics, stem, tokenize
import functions as f
import editdist as ed
#from metaphone import doublemetaphone
import phonetics as p

stemmer = stem.PorterStemmer()
TRIPLES = {
    'dge': 'j',
    'dgi': 'j',
    'dgy': 'j',
    'sia': '+x',
    'sio': '+x',
    'tia': '+x',
    'tio': '+x',
    'tch': '',
    'tha': '0',
    'the': '0',
    'thi': '0',
    'tho': '0',
    'thu': '0',
    }

DOUBLES = {
    'ph' : 'f',
    'sh' : 'x'
    }

SINGLETONS = {
    'd': 't',
    'f': 'f',
    'j': 'j',
    'l': 'l',
    'm': 'm',
    'n': 'n',
    'r': 'r',
    'p': 'p',
    'q': 'k',
    'v': 'f',
    'x': 'ks',
    'z': 's',
}

def normalize(s):
    words = tokenize.wordpunct_tokenize(s.lower().strip())
    return ' '.join([stemmer.stem(w) for w in words])
 
def fuzzy_match(s1, s2, max_dist=3):
    return metrics.edit_distance(normalize(s1), normalize(s2)) <= max_dist

def fz_token_ratio(lay1, lay2):
    pertinencia_soma = 0
    pertinencia_qtd = 0
    for key, valor in lay1.items():
        if key in lay2:
            pertinencia_qtd+=1
            #pertinencia_soma+=fuzz.partial_ratio(valor, lay2[key])
            pertinencia_soma+=fuzz.token_set_ratio(valor, lay2[key])
            #print process.extractOne(valor, lay2[key])
    return pertinencia_soma/pertinencia_qtd

def QRatio(lay1, lay2):
    return fuzz.QRatio(lay1, lay2)


def fz_extract_one(str1, listWords):
    return process.extractOne([str1], listWords)

def soundex(s):
    return p.soundex(term)

def metaphone(s):
    return p.metaphone(s)

def nysiis(s):
    return p.nysiis(s)

def caverphone(s):
    return p.caverphone(s)

def editdistance(str1, atr2):
    return ed.distance(str1, atr2)

def palavraCorreta(word):
    return f.correctText(word)
