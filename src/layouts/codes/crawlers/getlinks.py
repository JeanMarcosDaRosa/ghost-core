#getlinks.py

from BeautifulSoup import BeautifulSoup, SoupStrainer

db = MongoClient().ghost

def run(dispach, logger, procName, url, root, defproduto, defcategoria, retorno=None):
    logger.info("EXEC ["+procName+"] url: "+url)
    
    obj=db.collect_links.find_one({"_id": url})
    
    if obj!=None and obj['indexed']:
        obj = db.collect_links.find_one({"indexed": False, "inproc": False})
        if obj==None:
            dispach(procName, False, True)
            return
        else:
            url = obj["_id"]
        
    logger.info("GET URL -> ["+url+"]")
    
    if obj!=None: db.collect_links.update({"_id": url},{"$set": {"inproc": True}})
    pagina = su.getPage(url)
    if pagina!=None:
        try:
            soup = BeautifulSoup(pagina.read())
            links = soup.findAll('a')
            for a in links:
                if a.has_key('href'):
                    lk=su.trim(a['href'])
                    if lk != '#':
                        if (defproduto in lk and lk.startswith('http')):
                            obj = db.collect_links_product.find_one({"_id": su.trim(lk)})
                            if obj==None:
                                logger.info("collected product link: "+lk)
                                db.collect_links_product.insert({"_id": su.trim(lk), "root": root, "indexed": False, "inproc": False})
                        elif (defcategoria in lk and lk.startswith('http')):
                            logger.info("found category link: "+lk)
                            obj = db.collect_links.find_one({"_id": su.trim(lk)})
                            if obj==None:
                                logger.info("collected category link: "+lk)
                                db.collect_links.insert({"_id": su.trim(lk), "root": root, "indexed": False, "inproc": False})
        except Exception, e2:
            logger.error(e2)
        finally:
            pagina.close()
        db.collect_links.update({"_id": url},{"$set": {"root": root, "indexed": True}}, upsert=True)
    dispach(procName, False, True)
