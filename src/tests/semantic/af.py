#!/usr/bin/env python
#-*- coding:utf-8 -*-

( ALFABETO, GRAMATICA, ESTADO_INICIAL, ESTADOS_FINAIS ) = range(4)

def validate_string(alphabet, string):
    for char in string:
        if char not in alphabet:
            print 'validate_string: '\
                  '"%s" eh uma palavra invalida no alfabeto {%s}' %\
                  (string, ','.join(alphabet))
            return False

    return True


def processar(maquina, string, estado, estados, posicao=0):
    c = string[posicao]
    pos = maquina[GRAMATICA][estado]
    for e in pos:
        for p in e:
            if p in estados:
                r = processar(maquina, string, p, estados, posicao)
                if not r:
                    break
            else:
                if c in maquina[GRAMATICA][p]:
                    print c
    return (estado in maquina[ESTADOS_FINAIS])

if __name__ == '__main__':
    alfabeto = set(['0', '1'])
    gramatica = {
        'S'  : [ ['SC'], ['SE'] ],
        'SE' : [ ['E', 'CE'], ['E', 'C'], ['E', 'SE'] ],
        'SC' : [ ['C'], ['C', 'SC'], ['C', 'S'] ],
        'CE' : [ ['C', 'E'], ['CE', 'C'] ],
        'C'  : [ ['0', '1', '1'], ['1', '0', '1'], ['1', '1', '0'], ['0', '0', '0']  ],
        'E'  : [ ['0', '0', '1'], ['1', '1', '1'], ['0', '1', '0'], ['1', '0', '0'] ]
    }
    estado_inicial = 'S'
    estados_finais = set(['C'])
    maquina = (alfabeto, gramatica, estado_inicial, estados_finais)
    strings = [
        '001010110',	#correto
        '1101',			#invalido
        '011101001110',	#correto
        '101001010110',	#correto
    ]
    for string in strings:
        if validate_string(maquina[ALFABETO], string):
            estados = set(maquina[GRAMATICA].keys())
            estado = maquina[ESTADO_INICIAL]
            result = processar(maquina, string, estado, estados)
            if result != None:
                print '%s = %s' % (string, result)
