#construct_layout.py

WORDS_DEF = []
WORDS_DIC = []
WORDS_ASC = []
lista_dic = []
GRAMMARS  = []
changeDic = False

def layoutReader(filepath, filename, logger):
    global WORDS_DEF
    gramatica= ""
    palavras = []
    conexoes = []
    filters  = []
    tabelas  = []
    extra = []
    try:
        logger.info("Lendo layout["+str(filepath+filename)+"] ")
        lay = su.readJsonFile(filepath+filename)
        logger.info("Construindo layout["+str(lay["nome"])+"] "+str(lay["descricao"]))
        DEFs = []
        for atr in lay["atributos"]:
            for k,v in atr.items():
                DEFs.append(str(k))
        for atr in lay["atributos"]:
            if type(atr) == dict:
                #para cada definicao gramatical
                for k, v in atr.items():
                    #print k,v
                    first = True
                    if type(v) == dict:
                        dt = v['DT']
                        if type(dt) == list:
                            gramatica+='\n'+k+' -> '
                            #para cada regra
                            for r in dt:
                                #print r
                                if first==False:
                                    gramatica+="| "
                                #para cada palavra ou definicao na regra
                                for w in r:
                                    if type(w) == type("") or type(w) == unicode:
                                        #para uma simpes string
                                        if w in DEFs:
                                            gramatica+=w+" "
                                        else:
                                            palavras.append(su.utf8lower(w))
                                            gramatica+="'"+su.utf8lower(w)+"' "
                                    elif type(w) == dict:
                                        #para um conjunto definido por um prefixo e seus sufixos
                                        for pref, sufs in w.items():
                                            firstIn = True
                                            for s in sufs:
                                                if firstIn==False:
                                                    gramatica+="| "
                                                palavras.append(su.utf8lower(pref+s))
                                                gramatica+="'"+su.utf8lower(pref+s)+"' "
                                                firstIn=False
                                first=False
                        if 'MC' in v:
                            MCs = v['MC']
                            if type(MCs) == list:
                                l=[]
                                for mc in MCs:
                                    if mc['type'] == 'append':
                                        code = su.readFileCode(__layouts__, "microcall", mc['exec']+".py")
                                        appendRet = k+'_'+str(current_milli_time())
                                        variaveis[appendRet] = None
                                        params=None
                                        if 'params' in mc:
                                            params=mc['params']
                                        code += '\n\nrun("%s", params)' % appendRet
                                        exec(code, globals(), locals())
                                        if variaveis[appendRet]!=None:
                                            for x in variaveis[appendRet]:
                                                if first==False:
                                                    gramatica+="| "
                                                palavras.append(su.utf8lower(x))
                                                gramatica+="'"+su.utf8lower(x)+"' "
                                                first=False

                                    if mc['type']=='filter':
                                        l.append(mc['exec']+".py")
                                filters.append({k:l})
                        if 'CX' in v:
                            CXs = v['CX']
                            if type(CXs) == list:
                                l=[]
                                for cx in CXs:
                                    if type(cx) == type("") or type(cx) == unicode:
                                        c = su.readJsonFile(filepath+cx)
                                        logger.info("Ontologia: conexao entre os objetos ["+str(lay["nome"])+", "+str(c['nome'])+"] foi encontrada")
                                        l.append(c['nome'])
                                conexoes.append({k:l})
                        if 'BD' in v:
                            BDs = v['BD']
                            if type(BDs) == list:
                                l=[]
                                for bd in BDs:
                                    if type(bd) == type("") or type(bd) == unicode:
                                        l.append(bd)
                                tabelas.append({k:l})

    except Exception, e:
        logger.error(e)
        
    for p in palavras:
        WORDS_DEF.append(p)

    return (lay["nome"], gramatica, palavras, {"MC":filters, "CX": conexoes, "BD": tabelas})


def getLayoutOnto(folder,logger):
    global GRAMMARS
    try:    
        for root, subdirs, files in os.walk(folder):
            for arq in files:
                GRAMMARS.append(layoutReader(root,arq,logger))
            for subdir in subdirs:
                getLayoutOnto(subdir,logger)
    except Exception, e:
        pass


def leDiretorioDicionarios(folder,logger):
    global lista_dic, changeDic
    for root, subdirs, files in os.walk(folder):
        for arq in files:
            if not root+arq in lista_dic:
                changeDic = True
                lista_dic.append(root+arq)
                leDicionario(root+arq, logger)
        for subdir in subdirs:
            leDiretorioDicionarios(subdir, logger)


def leDicionario(dicionario,logger):
    global WORDS_DIC
    if dicionario.endswith('.dic'):
        content=None
        logger.info("Processando dicionario ["+dicionario+"]")
        with open(dicionario) as f:
            content = f.readlines()
        for i in content:
            x = i.replace('\n', '')
            if len(x) > 1 and not su.isnum(x):
                WORDS_DIC.append(su.utf8lower(x))


def run(dispach,logger,procName,__layouts__,layout,usa_produto=False,dicionario=None):
    global WORDS_DIC, WORDS_DEF, lista_dic, GRAMMARS, changeDic, WORDS_ASC
    logger.info("EXEC ["+procName+"]")
    logger.info("Target Layout ["+layout+"]")
    changeDic = False

    if 'lista_dicionarios' in variaveis:
        lista_dic = variaveis['lista_dicionarios']
        logger.info("Reuso da lista de dicionarios")
    if 'dicionarios' in variaveis:
        WORDS_DIC = variaveis['dicionarios']
        logger.info("Reuso da lista de palavras de dicionarios")
    if 'dicionarios_ascii' in variaveis:
        WORDS_ASC = variaveis['dicionarios_ascii']
        logger.info("Reuso da lista de palavras de dicionarios com remocao dos acentos")

    getLayoutOnto(__layouts__ + 'ontology/', logger)
    #print json.dumps( ontologia, ensure_ascii=False, sort_keys=True, indent=4, separators=(',', ': ') )

    #print GRAMMARS
    #exit(0)
    #su.debug(GRAMMARS)
    if usa_produto:
        db = MongoClient().ghost
        for p in db.collect_product.find():
            for t in nltk.word_tokenize(p['titulo']):
                if len(t) > 1 and not su.isnum(t):
                    WORDS_DEF.append(su.utf8lower(t))
            for i in p['categorias']:
                for x in i:
                    for t in nltk.word_tokenize(x):
                        WORDS_DEF.append(su.utf8lower(t))
            for x in p['definicoes']:
                for t in nltk.word_tokenize(x):
                    if len(t) > 1 and not su.isnum(t):
                        WORDS_DEF.append(su.utf8lower(t))
                for t in nltk.word_tokenize(p['definicoes'][x]):
                    if len(t) > 1 and not su.isnum(t):
                        WORDS_DEF.append(su.utf8lower(t))
                        
    if dicionario != None and dicionario != '':
        if os.path.exists(dicionario):
            if os.path.isfile(dicionario):
                leDicionario(dicionario, logger)
            else:
                leDiretorioDicionarios(dicionario, logger)

    WORDS_DEF = set(WORDS_DEF)
    if changeDic:
        WORDS_DIC = set(WORDS_DIC)
        WORDS_ASC = {}
        for x in WORDS_DIC:
            WORDS_ASC[su.removeAcentos(x)] = x

    dispach(procName, {'ontologia':GRAMMARS, 'definicoes': WORDS_DEF, 'lista_dicionarios': lista_dic, 'dicionarios': WORDS_DIC, 'dicionarios_ascii': WORDS_ASC}, True)

#__layouts__ = "../../"
#run(None,None,"construct_layout",__layouts__,"layout_necessidade")