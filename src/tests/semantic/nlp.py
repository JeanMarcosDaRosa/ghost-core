#coding latin-1
import nlpnet, json, sys
#nlpnet.set_data_dir('/home/jean/Programacao/tcc/src/mining/projects/ghost-core/src/semantic/data/pos-pt')
nlpnet.set_data_dir('/home/jean/Programacao/tcc/src/mining/projects/ghost-core/src/semantic/data/srl-pt')
#tagger = nlpnet.POSTagger()
#print tagger.tag('O rato roeu a roupa do rei de Roma.')
#[[(u'O', u'ART'), (u'rato', u'N'), (u'roeu', u'V'), (u'a', u'ART'), (u'roupa', u'N'), (u'do', u'PREP+ART'), (u'rei', u'N'), (u'de', u'PREP'), (u'Roma', u'NPROP'), (u'.', 'PU')]]

reload(sys)
sys.setdefaultencoding('Cp1252')

tagger = nlpnet.SRLTagger()
frazes = [
    'meu irmão não quer comprar uma TV LED',
    'não quero comprar uma TV Sansung, alguém tem alguma para me vender?',
    'o meu irmao quer uma TV LED',
    'meu irmão Mateus, está procurando uma TV de LED, alguém tem?',
    'quem tem uma TV LED pra me vender?',
    'quero uma TV de preferência sansung e LED, alguém?',
    'sera que é bom?',
    'sera que o novo moto g é tão bom assim?',
    'eim Mateus, o que tu acha desta?',
    'vendo TV LED sansung, R$1000',
    'quero uma TV nova, mas tem que ser da sansung, e de LED, de preferencia tela grande, maior que 40 polegadas.',
    'o eduardo e a monica querem comprar uma casa, alguém ai tem uma pra vender pra eles?',
    'quero comprar uma TV de marca boa'
]
frazes2=[]
for f in frazes:
  frazes2.append(f)

cont = 0
for f in frazes2:
  print "Frase:", f
  sents = tagger.tag(f)
  for sent in sents:
    if len(sent.arg_structures)>0:
      cont+=1
      print sent.tokens
      print json.dumps( sent.arg_structures, ensure_ascii=False, sort_keys=True, indent=4, separators=(',', ': ') )

print "Acertos "+str(cont)+"/"+str(len(frazes2))


nlpnet.set_data_dir('/home/jean/Programacao/tcc/src/mining/projects/ghost-core/src/semantic/data/pos-pt')
tagger = nlpnet.POSTagger()
for f in frazes2:
  print tagger.tag(f)
