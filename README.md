# Ghost Core #

### O que é o Ghost Core ###
Núcleo de processamento de linguagem natural.
Desenvolvido para atender ao aplicativo Social Ghost, com o intuito de atender as demandas de publicação de postagens e comentários, para interpretação semântica das informações e busca de objeto de interesse do usuário. Descrito no artigo [Processamento de linguagem natural para mineração de interesses em redes sociais](https://bytebucket.org/JeanMarcosDaRosa/ghost-core/raw/d6c3d077f26225044d45ecc44ce371155d69eb80/docs/artigo/ArtigoTCC-JeanMarcosdaRosa.pdf?token=1523336c727e4c1840a7934c4163e22a4683d02a)

### Tecnologias Empregadas ###
* Redes Neurais
* Ontologia de Gramática
* MongoDB
* NLTK
* Python