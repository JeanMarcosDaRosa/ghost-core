#!/usr/bin/python

import logging
import sys
import time
import thread
import utils as su
from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime, timedelta
from pymongo import MongoClient
from bson.son import SON
from bson.objectid import ObjectId
from pprint import pprint
from BeautifulSoup import BeautifulSoup
from fuzzywuzzy import fuzz
from nltk import CFG
from nltk.tree import *
from nltk.draw import *
import nltk
import os
import os.path
import json
import nlpnet
import hashlib
import fuzzy as fz
import copy
import re

reload(sys)
sys.setdefaultencoding('Cp1252')

__author__  = "jean"
__date__    = "$28/03/2015 00:36:21$"
__layouts__ = "../layouts/"

print '''
          ____ _   _  ___  ____ _____    ____ ___  ____  _____
         / ___| | | |/ _ \/ ___|_   _|  / ___/ _ \|  _ \| ____|
        | |  _| |_| | | | \___ \ | |   | |  | | | | |_) |  _|
        | |_| |  _  | |_| |___) || |   | |__| |_| |  _ <| |___
         \____|_| |_|\___/|____/ |_|    \____\___/|_| \_\_____|
    '''

'''
mapeamento dos logs
'''
logger = logging.getLogger("semantic.py")
current_milli_time = lambda: int(round(time.time() * 1000))
'''
mapeamento das variaveis
'''
fluxoConfig = None
variaveis = {}
processos = {}
filaProc  = {}
scheduler = BackgroundScheduler()

'''
definicoes dos metodos
'''

def configGhostSemantic():
    global logger, formatter
    config = su.readJsonFile('configuracao.json')
    formatter = logging.Formatter(config['logger_format'])
    exec("logger.setLevel(logging."+config['logger_level']+")", globals(), globals())
    ch = logging.StreamHandler()
    exec("ch.setLevel(logging."+config['logger_level']+")", locals(), globals())
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    #debug,info,warn,error,critical

def controlaFila(sleepTime=1):
    global processos, filaProc
    while True:
        for nome, proc in filaProc.items():
            for i in proc['instancias']:
                if processos[nome]['threads'] < processos[nome]['maxthreads']:
                    try:
                        logger.debug("FILA REM [" + nome + "] ")
                        code = processos[nome]['code']
                        code += i['params']
                        thread.start_new_thread(execCode, (code, nome))
                        filaProc[nome]['instancias'].remove(i)
                    except Exception as inst:
                        logger.error(inst)
        time.sleep(sleepTime)

def load_process(process_name):
    logger.debug('carregando processo: ' + process_name)
    return su.readJsonFile(__layouts__ + 'jobs/' + process_name + '.json')

def cloneLogger(log, nome):
    logc = copy.copy(log)
    logc.name = nome
    return logc

def execCode(code, nome):
    logger.info("executando processo: " + nome)
    processos[nome]['threads'] += 1
    try:
        exec (code, globals(), globals())
    except Exception, e:
        logger.error("CALL: "+nome+": "+str(e.message))

def callExec(proc, argumentos=None, appendRet=None):
    global processos, filaProc
    code = su.cleanCode(proc['code'])
    params = 'dispach,cloneLogger(logger, "'+proc['nome']+'")'+',"'+proc['nome']+'"'
    for p in proc['parametros']:
        if type(p) is dict:
            if argumentos!=None:
                for key, value in p.items():
                    if value in argumentos:
                        if type(argumentos[value]) is dict:
                            for keyA, valueA in argumentos[value].items():
                                params += ',' + keyA+'='+valueA
                        else:
                            params += ',' + key + '=' + argumentos[value]
                    else:
                        params += ',' + key+'='+value
            else:
                for key, value in p.items():
                    params += ',' + key+'='+value
        else:
            if argumentos!=None:
                if p in argumentos:
                    if type(argumentos[p]) is dict:
                        for key, value in argumentos[p].items():
                            params += ',' + key+'='+value
                    else:
                        params += ',' + argumentos[p]
                else:    
                    params += ',' + p
            else:          
                params += ',' + p

    if appendRet!=None and proc['getretorno']:
        params+=',retorno=variaveis["'+appendRet+'"]'
    parToRun = '\n\nrun(' + params + ')'
    if processos[proc['nome']]['threads'] >= processos[proc['nome']]['maxthreads']:
        logger.debug("FILA ADD [" + proc['nome'] + "] na fila com " + str(proc['threads']))
        if not proc['nome'] in filaProc:
            filaProc[proc['nome']] = {"instancias": [{"params": parToRun}]}
        else:
            filaProc[proc['nome']]['instancias'].append({"params": parToRun})
    else:
        try:
            code+=parToRun
            thread.start_new_thread(execCode, (code, proc['nome']))
        except Exception as inst:
            logger.error(inst)    
    
def execProc(procName):
    global processos, logger
    callExec(processos[procName])

def dispach(procName, argumentos=False, final=False):
    global processos, logger
    
    proc = processos[procName]
    logger.info("DISPACH: [" + procName + "], final: "+str(final))
    #logger.debug("Argumentos: "+str(argumentos))
    if final or argumentos==False:
        processos[procName]['threads'] -= 1
    
    if argumentos==False:
        logger.info("processo: " + procName + " nao teve resultados e finalizou o fluxo")
        return
    
    localRet={}
    appendRet=None
    
    #pega os retornos
    if 'retorno' in proc and proc['retorno']!=False and proc['retorno']!=None and len(proc['retorno'])>0 :
        for ret in proc['retorno']:
            if type(ret) is dict:
                for key, value in ret.items():
                    if value in argumentos:
                        localRet[key] = argumentos[value]
                    
    #pega os retorno globais
    if 'retorno_global' in proc and proc['retorno_global']!=False and proc['retorno_global']!=None and len(proc['retorno_global'])>0 :
        for ret in proc['retorno_global']:
            if type(ret) is dict:
                for key, value in ret.items():
                    if value in argumentos:
                        variaveis[key] = argumentos[value]
    
    if len(localRet) > 0:
        appendRet = procName+'_'+str(current_milli_time())
        variaveis[appendRet] = localRet
        
    for target in proc['subprocessos']:
        if target['nome'] in processos:
            procTarget = processos[target['nome']]
            if procTarget['ativo']:
                if target['divisor']!=False:
                    x = argumentos[target['divisor']]
                    if type(x) == list:
                        for i in su.chunks(x, procTarget['maxthreads']):
                            sub = argumentos
                            sub[target['divisor']] = i
                            callExec(procTarget, sub, appendRet)
                    else:
                        logger.info("DIVISOR invalido para o processo ["+target['nome']+"]: "+str(x))
                else:
                    callExec(procTarget, argumentos, appendRet)
            
def configScheduler(proc):
    global processos, scheduler
    prog = proc['programacao']
    atrazo = None
    if prog['autorun']:
        atrazo = (datetime.now() + timedelta(seconds=1))
        
    if prog['tipo']=='instancias':
        for i in range(0, prog['instancias']):
            scheduler.add_job(execProc, next_run_time=atrazo, args=[proc['nome']], id=proc['nome'])

    elif prog['tipo']=='interval':
        scheduler.add_job(execProc, prog['tipo'], seconds=prog[prog['tipo']], next_run_time=atrazo, args=[proc['nome']], id=proc['nome'])           
                
    elif prog['tipo']=='cron':
        scheduler.add_job(execProc, prog['tipo'], next_run_time=atrazo, args=[proc['nome']], id=proc['nome'])

    elif prog['tipo']=='date':
        scheduler.add_job(execProc, prog['tipo'], next_run_time=atrazo, args=[proc['nome']], id=proc['nome'])
                    
def execFluxos(conf):
    global processos, scheduler
    for proc in conf['processos']:
        if proc['ativo']:            
            logger.info("executando fluxo: " + conf['nome'])
            processos[proc['nome']] = proc
            processos[proc['nome']]['code'] = su.readFileCode(__layouts__, proc['categoria'], proc['arquivo'])
            processos[proc['nome']]['threads'] = 0
            if proc['programacao']!=False:
                configScheduler(proc)
    scheduler.start()
    try:
        thread.start_new_thread(controlaFila, ())
        while True:
            time.sleep(1)
    except Exception,e:
        scheduler.shutdown()

def run():
    global fluxoConfig
    
    #pega a configuracao geral no arquivo json
    configGhostSemantic()
    logger.info('******** Iniciando processamento semantico *******')
    #pega a configuracao dos processos no arquivo json
    if len(sys.argv) > 1:
        process = sys.argv[1]
        fluxoConfig = load_process(process)
        #logger.debug(fluxoConfig)
        if fluxoConfig != None:
            execFluxos(fluxoConfig)
        else:
            logger.critical('nao foi possivel definir os fluxos do processo')
            exit(1)
    else:
        logger.error("use: python " + sys.argv[0] + " [nome do processo]")

'''
Inicia a aplicacao / engine de processamento semantico
'''

run()