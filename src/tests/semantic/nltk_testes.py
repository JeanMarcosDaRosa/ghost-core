#coding latin-1

import nltk
from nltk import Nonterminal, nonterminals, Production, CFG, grammar, parse, RecursiveDescentParser, word_tokenize
import nltk.tag, nltk.data
from nltk.corpus import floresta

default_tagger = nltk.data.load(nltk.tag._POS_TAGGER)
def simplify_tag(t):
    if "+" in t:
        return t[t.index("+")+1:]
    else:
        return t

def utf8lower(str1):
    try:
        res=str1.decode('utf-8').lower()
    except Exception, e:
        pass
    return res

sentencas = [
    'quero comprar uma tv sansung, alguem tem alguma para me vender?',
    'meu irmao quer comprar uma tv led',
    'o meu irmao quer uma tv led',
    'meu irmao Mateus, esta procurando uma tv led, alguem tem?',
    'quem tem uma tv led pra me vender?',
    'quero uma tv de preferencia sansung e led, alguem?',
    'sera que e bom?',
    'sera que o novo moto g e tao bom assim?',
    'eim mateus, o que tu acha desta?',
    'vendo tv led sansung, R$1000',
    'quero uma tv nova, mas tem que ser da sansung, e de led, de preferencia tela grande, maior que 40 polegadas.',
    'o eduardo e a monica querem comprar uma casa, alguem ai tem uma pra vender pra eles?',
    'quero comprar uma tv de marca boa',
    'quero comprar uma tv, marca boa',
]

def ie_preprocess(document):
    sentences = nltk.sent_tokenize(document)
    sentences = [nltk.word_tokenize(sent) for sent in sentences]
    sentences = [nltk.pos_tag(sent) for sent in sentences]
    return sentences


grammar = CFG.fromstring("""
INICIO -> INTENCAOCOMPRA
INTENCAOCOMPRA -> SUGEITO INTENCAO | INTENCAO SUJEITO
SUGEITO -> REF PARENTE | PARENTE REF | 'eu' | 
INTENCAO -> DESEJO OBJETO | NECESSIDADE OBJETO
REF -> 'meu' | 'minha' 
PARENTE -> 'irmao' | 'irma' | 'tia' | 'tiu' | 'pai' | 'mae' | 'primo' | 'prima'
DESEJO -> 'quero' | 'quer' | 'queria' | 'desejo' | 'amo'
NECESSIDADE -> 'necessito'
OBJETO -> OUTROS 'telefone' | OUTROS 'tv'
OUTROS -> 'um' | 'dessas' | 'uma'
""");

#parser = RecursiveDescentParser(grammar)
parser = nltk.ChartParser(grammar)
for sent in sentencas:
    #print ie_preprocess(sent)
    tokens = nltk.word_tokenize(sent)
    #for tree in parser.parse(tokens):
    #    print(tree)

def showExtract(tagger, sentences):
    good,total = 0,0.
    for sentence in sentences:
        tags = tagger.tag(nltk.word_tokenize(sentence))
        print tags

def evaluate(tagger, sentences):
    good,total = 0,0.
    for sentence,func in sentences:
        tags = tagger.tag(nltk.word_tokenize(sentence))
        print tags
        good += func(tags)
        total += 1
    print 'Accuracy:',good/total

#sentences = [
#    ('quero uma tv nova', lambda tags: ('quero', 'VB') in tags and ('irmao', 'PAR') in tags),
#    ('meu irmao quer uma tv', lambda tags: ('meu', 'REF') in tags and ('irmao', 'PAR') in tags)
#]

'''
NED = desejo, necessidade, vontade...
REF = referencia
REF+D = referencia a algo a direira
IND = individuo
RI = referenciando um individo
RIE = referenciando um individo a esquerda
RID = referenciando um individo a direreita
ACT
'''

train_sents = [
    [('irmao', 'IND'), ('quer', 'NED'), ('comprar', 'ACT')]
]

print '============================================='
#train_sents = floresta.tagged_sents()
tagger = nltk.TrigramTagger(train_sents, backoff=None)
#ClassifierBasedPOSTagger
showExtract(tagger, sentencas)
#evaluate(tagger, sentences)
#model = tagger._context_to_tag