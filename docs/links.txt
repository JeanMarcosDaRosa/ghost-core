Links

http://text-processing.com/demo/tag/
http://www.nltk.org/
http://www.nltk.org/howto/classify.html
http://nlp.stanford.edu/nlp/javadoc/javanlp/edu/stanford/nlp/classify/package-frame.html
http://stackoverflow.com/questions/14716437/nltk-classify-interface-using-trained-classifier
http://nltk-trainer.readthedocs.org/en/latest/train_classifier.html
http://www.ibm.com/developerworks/br/library/os-pythonnltk/
http://textminingonline.com/tag/nltk
http://show.newscodes.org/index.html?newscodes=subj&lang=en-GB
https://github.com/EduardoCarvalho/nltkPhraseDetector
https://github.com/sciruela/NLTK-Sentiment-Analysis-Twitter
https://github.com/CompLin/Aelius

https://pypi.python.org/pypi/watchdog
http://brunorocha.org/python/watching-a-directory-for-file-changes-with-python.html
https://github.com/gorakhargosh/watchdog/

http://streamhacker.com/2011/10/31/fuzzy-string-matching-python/

PLN
http://ronan.collobert.com/senna/
http://143.107.183.175:21380/portlex/index.php/en/projects/verbnetbringl
http://www.teses.usp.br/teses/disponiveis/55/55134/tde-19042013-160640/pt-br.php
http://verbs.colorado.edu/verb-index/index.php
http://143.107.183.175:21380/portlex/images/arquivos/propbank-br/propbank.br%20tutorial.pdf
http://143.107.183.175:21380/portlex/index.php/pt/component/content/article/2-uncategorised/17
http://www.clips.uantwerpen.be/conll2000/chunking/

Image Recognition
http://coding-robin.de/2013/07/22/train-your-own-opencv-haar-classifier.html
https://realpython.com/blog/python/face-detection-in-python-using-a-webcam/