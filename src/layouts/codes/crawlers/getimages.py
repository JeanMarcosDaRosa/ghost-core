#getimages.py

#implementar reducao do tamanho da imagem para facilitar no armazenamento
def run(dispach, logger, procName, pathtosave, reprocessa=False, retorno=None):
    logger.info("EXEC ["+procName+"] pathtosave: "+pathtosave)
    try:
        obj=db.collect_product.find_one({"downimg": False, "indownimg": False})
        if obj != None and 'imagem' in obj and obj['imagem']!="":
            logger.debug("Buscando imagem para ["+obj["_id"]+"]")
            link=obj['imagem']
            db.collect_product.update({"_id": obj["_id"]},{"$set":{"indownimg": True}})
            m = hashlib.md5()
            m.update(link)
            hash_str = m.hexdigest()
            nomearq=pathtosave+"/"+hash_str+".jpg"            
            if reprocessa or not os.path.exists(nomearq):
                if os.path.exists(nomearq):
                    os.remove(nomearq)
                logger.info("GETTING IMAGE: ["+link+"] -> "+nomearq)
                img = su.getPageWithCookies( link.decode("latin-1") )
                if img != None:
                    if not os.path.exists(pathtosave):
                        try:
                            os.mkdir(pathtosave)
                        except:
                            pass
                    output = open(nomearq,'wb')
                    output.write(img.read())
                    output.close()
                logger.info("IMAGE: ["+link+"] OK")
                db.collect_product.update({"_id": obj["_id"]},{"$set":{"downimg": True}})
        dispach(procName,False,True)
    except Exception,e:
        print 'Error:', e
        dispach(procName,False,True)
