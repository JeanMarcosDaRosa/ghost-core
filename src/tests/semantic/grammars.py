#coding latin-1

import nltk
from nltk import Nonterminal, nonterminals, Production, CFG, grammar, parse, RecursiveDescentParser, word_tokenize

sentencas = [
    'quero um notebook de preferência lenovo e preço até 2 mil reais'
]

g1="\nIND -> DEF | REF | REF | CONJ | REF | DEF | CONJ | REF | REF | CONJ | DEF \nDEF -> 'eu' | 'algu\xe9m' | 'voc\xea' | 'vc' \nCONJ -> 'e' | ',' \nREF -> 'meu' | PAR | 'minha' | PAR | 'teu' | PAR | 'tua' | PAR \nPAR -> 'irm\xe3o' | 'irm\xe3' | 'm\xe3e' | 'pai' | 'tiu' | 'tia' | 'primo' | 'prima' "
g2="\nOBJ -> NOME | NOME | QL | NOME | MARCA | MARCA | NOME | CAT | MARCA | CAT | NOME | CAT | MARCA | QL | CAT | NOME | QL \nNOME -> 'inspiron' | 's4' | 'moto' | 'g' | 'moto' | 'x' | 'modelo' | NOME \nCAT -> 'note' | 'notebook' | 'informatica' | 'smartphone' | 'tv' | 'celular' \nMARCA -> 'motorola' | 'samsung' | 'lg' | 'marca' | MARCA | 'marca' | QL \nQL -> 'bom' | 'boa' | '\xf3timo' | 'perfeito' | 'ruim' | 'p\xe9ssimo' | 'quebrado' | 'pifado' | 'novo' "
g3="\nCOR -> DEF | DEF | TOM | REF | TOM \nDEF -> 'azul' | 'azulado' | 'azulada' | 'branca' | 'branco' | 'brancas' | 'brancos' | 'vermelha' | 'vermelho' \nTOM -> 'claro' | 'escuro' \nREF -> 'cor' | 'cores' | 'cor' | DEF | TOM | 'na' | 'nas' | REF | 'da' | 'das' | REF | 'em' | DEF | 'em' | DEF | TOM " 
g4="\nINT -> DSJ | ACT_C | ACT_V | DSJ | ACT_C | DSJ | ACT_V \nDSJ -> 'quer' | 'quero' | 'queria' | 'quis' | 'amo' | 'desejo' | 'preciso' | 'precisa' | 'precisava' | 'precisei' | 'precisamos' | 'procuro' | 'procura' | 'procuramos' | 'procurava' | 'tenho' | 'que' | ACT_C | 'tenho' | 'que' | ACT_V \nACT_C -> 'compra' | 'compro' | 'compramos' | 'comprei' | 'comprarei' | 'comprara' | 'ter' | 'ganhar' \nACT_V -> 'venda' | 'vendo' | 'vendemos' | 'venderei' | 'vendi' | 'ter' | 'ganhar' "


g="""
INICIO -> INT
INT -> 'abc'
AAA -> 'xxx'
"""
print g.decode('utf8')

grammar = CFG.fromstring(g.decode('utf8'));

print "INICIO================================================"

#parser = RecursiveDescentParser(grammar)
parser = nltk.ChartParser(grammar)
for sent in sentencas:
    #print ie_preprocess(sent)
    tokens = nltk.word_tokenize(sent.decode("utf8"))
    for tree in parser.parse(tokens):
        print(tree)

print "FIM==================================================="

'''
NED = desejo, necessidade, vontade...
REF = referencia
REF+D = referencia a algo a direira
IND = individuo
RI = referenciando um individo
RIE = referenciando um individo a esquerda
RID = referenciando um individo a direreita
ACT
'''
#DependencyGrammar