#analize.py

ONTOLOGY = {}
FUZZY_MATCHS = {}

def match(str1, str2, algorithm=['fuzzywuzzy', 'fz_token_ratio'], validate=None, logger=None):
    _retorno = False
    if type(str2) == type(''):
        logger.debug("Fuzzy Algorithm: "+str(algorithm)+" to ["+str(str1)+","+str2+"]")
    else:
        logger.debug("Fuzzy Algorithm: "+str(algorithm)+" to ["+str(str1)+"]")
    if algorithm[0]=='phonetic':
        if algorithm[1] == 'soundex':
            x,y=fz.soundex(str1),fz.soundex(str2)
            logger.debug("soundex["+str1+"="+x+", "+str2+"="+y+"]")
            _retorno = x == y
    elif algorithm[0]=='distance':
        if algorithm[1] == 'levenshtein':
            x=fz.editdistance(str1,str2)
            logger.debug("levenshtein["+str1+", "+str2+"="+str(x)+" max_dist="+str(validate))
            _retorno = x <= validate
        if algorithm[1] == 'nltk':
            x=fuzzy_match(str1, str2, max_dist=validate)
            logger.debug("nltk["+str1+", "+str2+" (max_dist)="+str(x)+"] max_dist="+str(validate))
            _retorno = x            
    elif algorithm[0]=='fuzzywuzzy':
        if algorithm[1] == 'fz_token_ratio':
            if validate==None or validate>100: validate=100
            x=fz.fz_token_ratio({"str": str1}, {"str": str2})
            logger.debug("fz_token_ratio["+str1+", "+str2+"="+str(x)+"] pertinency="+str(validate))
            _retorno =  x >= validate
        if algorithm[1] == 'fz_extract_one':
            if validate==None or validate>100: validate=100
            x=fz.fz_extract_one(str1, str2)
            logger.debug("fz_extract_one["+str1+" -> "+x[0]+"="+str(x[1]>validate)+"] = pertinency="+str(x[1]))
            _retorno =  x
        if algorithm[1] == 'QRatio':
            if validate==None or validate>100: validate=100
            x=fz.QRatio(str1, str2)
            logger.debug("QRatio["+str1+", "+str2+"] pertinency="+str(x))
            _retorno =  x >= validate
    elif algorithm[0]=='en_metaphone':
        if algorithm[1] == 'single':
            x,y=fz.metaphone(str1), fz.metaphone(str2)
            logger.debug("single["+str1+"="+x+", "+str2+"="+y+"]")
            _retorno = x==y
        if algorithm[1] == 'double':
            x,y=fz.doublemetaphone(str1) == fz.doublemetaphone(str2)
            logger.debug("double["+str1+"="+x+", "+str2+"="+y+"]")
            _retorno = x==y
    logger.debug("Result="+str(_retorno))
    return _retorno

def parseSentence(sentenca, wordsDic, wordsAscii, wordsOnto, algoritmo, pertinencia=None, fonetico=None, logger=None):
    global FUZZY_MATCHS
    newSentence = []
    ax = algoritmo.split(':')
    if algoritmo=='fuzzywuzzy:fz_extract_one':
        for p in nltk.word_tokenize(sentenca):
            if len(p)>1 and not su.isnum(p):
                if p in wordsDic:
                    newSentence.append((p, True))
                else:
                    if p in wordsAscii:
                        newSentence.append((wordsAscii[p], True))
                    else:
                        if p in FUZZY_MATCHS:
                            newSentence.append((FUZZY_MATCHS[p], True))
                        else:
                            if p in wordsOnto:
                                newSentence.append((p, True))
                            else:
                                x=match( p, wordsOnto, ax, pertinencia, logger )
                                if x!=False and x[1]>=pertinencia:
                                    FUZZY_MATCHS[p]=x[0]
                                    newSentence.append((x[0], True))
                                else:
                                    newSentence.append((p, False))
            else:
                newSentence.append((p, True))
    else:
        for p in nltk.word_tokenize(sentenca):
            if len(p)>1 and not su.isnum(p):
                if p in wordsDic:
                    newSentence.append((p, True))
                else:
                    if p in wordsAscii:
                        newSentence.append((wordsAscii[p], True))
                    else:
                        if p in FUZZY_MATCHS:
                            newSentence.append((FUZZY_MATCHS[p], True))
                        else:
                            if p in wordsOnto:
                                newSentence.append((p, True))
                            else:
                                aux=None
                                for w in wordsOnto:
                                    if match( p, w, ax, pertinencia, logger ): #if match( su.decodeUtf8(p), su.decodeUtf8(w), ax, pertinencia, logger ):
                                        aux=w
                                        break
                                if aux!=None:
                                    FUZZY_MATCHS[p]=aux
                                    newSentence.append( (aux, True) )
                                else:
                                    newSentence.append( (p , False) )
            else:
                newSentence.append((p, True))
    aux=[]    
    for t in newSentence:
        p,a = t
        if a==False:
            for x in fz.palavraCorreta(p):
                if x[0]!=False:
                    aux.append(x[0])
                elif fonetico!=None and fonetico!='':
                    fzf=fz.metaphone
                    if fonetico=='metaphone':
                        fzf=fz.metaphone
                    elif fonetico=='soundex':
                        fzf=fz.soundex
                    elif fonetico=='nysiis':
                        fzf=fz.nysiis 
                    elif fonetico=='caverphone':
                        fzf=fz.caverphone
                    logger.warn("Nao nao encontrou palavra compativel para ["+p+"] buscando em dicionario fonetico["+str(fonetico)+"]...")
                    #ultima tentativa, usando algoritmo fonetico com o dicionario conhecido
                    pf=fzf(p)
                    enc = False
                    for o in wordsOnto:
                        if pf==fz.metaphone(o):
                            logger.warn("encontrou palavra compativel para ["+p+"] = ["+o+"] em dicionario fonetico!")
                            enc=True
                            aux.append(o)
                            break
                    if not enc:
                        logger.warn("Nao nao encontrou palavra compativel para ["+p+"] em dicionario fonetico!")
        else:
            aux.append(p)
    return aux
    
def valGrammar(grammars, sent, logger):
    gFound = []
    for nome, g, words, ex in grammars:
        words2 = [su.utf8lower(x) for x in words]
        words = words2
        #print g
        grammar = CFG.fromstring(su.utf8(g))
        parser = nltk.ChartParser(grammar)
        tokens = [t for t in nltk.word_tokenize(sent) if t in words]
        if len(tokens)>0:
            logger.info("Tokens considerados para a gramatica["+str(nome)+"]: "+str(tokens))
            #if nome=='OBJ': su.debug(g)
            try:
                for tree in parser.parse(tokens):
                    if len(tree) > 0:
                        gFound.append(tree)
                        #break
            except:
                pass
        else:
            logger.info("Nenhum tokem foi considerado para a gramatica: "+str(nome))
    return gFound


def run(dispach,logger,procName,layout,algoritmo,pertinencia,fonetico,textoderecurso=False,salvarprocessado=False, nlpnet_tagger=None, nlpnet_data=None,retorno=[]):
    global FUZZY_MATCHS, variaveis
    logger.info("EXEC [inference] layout ["+layout+"] algorithm ["+algoritmo+"] | pertinence ["+str(pertinencia)+"]")

    if 'FUZZY_MATCHS' in variaveis:
        logger.debug("Reusando FUZZY_MATCHS")
        FUZZY_MATCHS=variaveis['FUZZY_MATCHS']

    tagger=None
    
    nlpnet.set_data_dir(str(nlpnet_data))
    if not 'tagger' in variaveis:
        if nlpnet_tagger=='srl':
            logger.info("Carregando rede neural para SRL Tagger com dados de ["+nlpnet_data+"]...")
            tagger = nlpnet.SRLTagger()
            logger.info("Rede neural carregada/treinada")
        elif nlpnet_tagger=='pos':
            logger.info("Carregando rede neural para POS Tagger com dados de ["+nlpnet_data+"]...")
            tagger = nlpnet.POSTagger()
            logger.info("Rede neural carregada/treinada")
    else:
        logger.debug("Reusando tagger")
        tagger = variaveis['tagger']
            
    if tagger==None:
        logger.info("Tagger esta nulo, processo de inferencia depende dele! verifique as configuracoes.")
        exit(0)    

    layOnto = retorno['ontologia']
    words = retorno['dicionarios']
    words_def = retorno['definicoes']
    words_asc = retorno['dicionarios_ascii']
    db = MongoClient().ghost
    for l in db.posts.find({"extractRes": True,"procSema": False}):    
        fraze = l['message']
        if textoderecurso and 'resources' in l:
            for r in l['resources']:
                if 'text' in r and r['text']!=None and r['text']!='':
                    logger.info("Adicionado sentenca encontrada em recurso, texto: ["+su.decodeUtf8(r['text'])+"]")
                    fraze+=' '+su.decodeUtf8(r['text'])
        sentencas = su.sentencesExtract(su.utf8lower(fraze))
        for sentenca in sentencas:
            novaFrase = parseSentence(su.utf8lower(sentenca), words, words_asc, words_def, algoritmo, pertinencia, fonetico, logger)
            if novaFrase != '':
                gFound=[]
                logger.info("Sentenca inicial: "+sentenca)
                novaFrase=' '.join(novaFrase)
                logger.info("Sentenca processada: "+novaFrase)
                sent = tagger.tag(novaFrase)[0]
                if len(sent.arg_structures)>0:
                    print json.dumps( sent.arg_structures, ensure_ascii=False, sort_keys=True, indent=4, separators=(',', ': ') )
                    for vb, st in sent.arg_structures:
                        #print "VERBO",vb
                        #TODO para cada verbo deve-se processar um contexto separado, mas por enquanto todos vao estar interligados em um unico contexto
                        if type(st) == dict:
                            for k,v in st.items():
                                logger.debug("Agrupamento:"+str(k)+" "+str(v))
                                z=valGrammar(layOnto, ' '.join(v), logger)
                                if len(z)>0:
                                    for x in z:
                                        gFound.append(x)
                    #print sent.arg_structures
                else:
                    logger.warn("Falha no tagger para a sentenca: "+novaFrase)
                    logger.debug("Validação por completo sendo iniciada ...")
                    z=valGrammar(layOnto, novaFrase, logger)
                    if len(z)>0:
                        for x in z:
                            gFound.append(x)
                if len(gFound)>0:
                    logger.debug("Enviando a arvore sintatica para processamento de inferencia")
                    descOnto = [(n,ex) for n, g, p, ex in layOnto]
                    #print gFound
                    #print descOnto
                    #exit(0)
                    dispach(procName, {'post': l['_id'], 'arvore_sintatica': gFound, 'ontologia': descOnto}, False)
                else:
                    logger.warn("Não foi possivel gerar a arvore sintatica para processamento de inferencia")
        if salvarprocessado:
            l['procSema']=True
            db.posts.save(l)
    dispach(procName, {'FUZZY_MATCHS':FUZZY_MATCHS, 'tagger': tagger}, True)