import os
import sys
import json

from pymongo import MongoClient
from bson.son import SON
db = MongoClient().ghost

walk_dir = sys.argv[1] if (len(sys.argv)>1) else '/home/jean/Programacao/WebSemantica/Processamento/mindnet/fuzzy'

print('Ontology Layout Folder (absolute) = ' + os.path.abspath(walk_dir))

db.ontology_map.remove()

arquivos=[]

def decodeUtf8(string):
    return string.decode('latin-1').encode('utf-8')

def openFolder(folder, level):
    level+=1
    for root, subdirs, files in os.walk(folder):
        for arq in files:
            if '.md' in arq:
                arquivos.append({"root":root,"file":arq})
        for subdir in subdirs:
            openFolder(subdir, level)
   
openFolder(walk_dir, 0)
onto = ''
ontoFullName = ''
ontologyMap = []
countDefs = 0
lay = {}
for a in arquivos:
    if ontoFullName != a['root']:
        ontoFullName=a['root']
        onto = ontoFullName.replace(walk_dir+'/', '').replace('.','_')
        if len(lay) > 0:
            ontologyMap.append(lay)
        lay = {'_id':onto,'layouts': []}
        #print '\n--------------------ONTO: [%s]--------------------' % onto
    obj = a['file'][:-3].replace('.','_')
    #print 'OBJ: %s' % obj
    lines = tuple(open(ontoFullName+'/'+a['file'], 'r'))
    mapaDef = {obj: []}
    for line in lines:
        line = line.strip(' \t\n\r')
        if line.startswith('dt '):
            defs = line[3:].split(',')
            if len(defs) > 0 and defs[0]!='':
                ldt = []
                for d in defs:
                    if not '.' in d:
                        dt={ decodeUtf8(d.strip(' \t\n\r$').lower()):{"pref":[],"sufx":[]} }
                        countDefs+=1
                        ldt.append(dt)
                mapaDef[obj].append(ldt)
        if line.startswith('suf'):
            prefs = line[3:].split(',')
            if len(prefs) > 0 and prefs[0]!='':
                lpx = []
                for d in prefs:
                    if d.strip(' \t\n\r')!='':
                        lpx.append(decodeUtf8(d.strip(' \t\n\r').lower()))
                mapaDef[obj].append({"prefixos": lpx})
    lay['layouts'].append(mapaDef)
    
if len(lay) > 0:
    ontologyMap.append({'_id':onto, 'layouts': mapaDef})
print '\nOntology[%s defs] in JSON:\n' % countDefs
print json.dumps( ontologyMap, ensure_ascii=False, sort_keys=True, indent=4, separators=(',', ': ') )

db.ontology_map.insert_many(ontologyMap)