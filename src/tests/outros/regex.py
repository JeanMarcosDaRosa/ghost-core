import re
import nltk

def extractUrlAndEmails(text):
    emails = re.findall(r'[\w\.-]+@[\w\.-]+', text)
    urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)
    return [ urls, emails ]

str1 = 'helo? my emails: 117687@upf.br, and jeanmarcosdarosa@gmail.com, contato@jeanmarcos.com.br, this is the message. review: http://url.com/123 http://url.com/45'

#print extractUrlAndEmails(str1)
