try:
    import Image
except ImportError:
    from PIL import Image

import pytesseract

def processImage(res,logger):
    if os.path.isfile(res):
        logger.debug('Extraindo texto[portugues] do recurso: ['+res+']')
        return pytesseract.image_to_string(Image.open(res), lang='por')
    else:
        logger.warn('Arquivo de recurso nao encontrado em: ['+res+']')
    return None

def run(dispach,logger,procName,pathres=None,pathnews=None,salvarprocessado=False):
    logger.info("EXEC ["+procName+"] pathres["+pathres+"]")
    db = MongoClient().ghost
    for l in db.posts.find({"extractRes": False, "procSema": False}):
        if 'resources' in l and l['resources']!=None and len(l['resources'])>0:
            logger.debug('Extraindo de '+str(l))
            countRes=0
            for i in l['resources']:
                logger.debug('Recurso encontrado: ['+i['source']+']')
                if i['type']=='photo':
                    if '[SERVER]' in i['source']:
                        res=i['source'].replace('[SERVER]/public/uploadpost', pathres)
                        i['text'] = processImage(res,logger)
                    else:
                        if i['source'].startswith('http'):
                            imagem_path=pathnews+'/'+str(l['_id'])+'_'+str(countRes)
                            if os.path.exists(imagem_path) and os.path.isfile(imagem_path):
                                i['text'] = processImage(imagem_path,logger)
                            else:
                                logger.info("GETTING IMAGE: ["+i['source']+"]")
                                img = su.getPageWithCookies( i['source'].decode("latin-1") )
                                if img != None:
                                    if not os.path.exists(pathnews):
                                        try:
                                            os.mkdir(pathnews)
                                        except:
                                            pass
                                    
                                    output = open(imagem_path,'wb')
                                    output.write(img.read())
                                    output.close()
                                    logger.info("DOWNLOAD IMAGE: ["+i['source']+"] -> ["+imagem_path+"] OK")
                                    i['text'] = processImage(imagem_path,logger)
                else:
                    logger.warn('Tipo de recurso nao suporta extracao de texto: ['+i['type']+']')
                countRes+=1
        if salvarprocessado:
            l['extractRes']=True
            db.posts.save(l)
    dispach(procName, False, True)
