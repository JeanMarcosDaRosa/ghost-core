import json
import urllib
import urllib2
import urlparse
import cookielib
import nltk
import keyword 
import re

from BeautifulSoup import BeautifulSoup, SoupStrainer

import re, sys, fileinput

from bisect import bisect_left

from unicodedata import normalize

def isVarName(str):
    return re.match("[_A-Za-z][_a-zA-Z0-9]*$",str) and not keyword.iskeyword(str)

def inValues(dic,value):
    for key,val in dic.iteritems():
        if value == val:
            return key
    return None

def removeAcentos(txt, codif='latin-1'):
    try:
        st=normalize('NFKD', txt.decode(codif)).encode('ASCII','ignore')
        return st
    except Exception,e:
        print e
     
def isnum(str1):
    if str1 == None : return False
    return str1.replace(',','').replace('.','',1).isdigit()

def biContains(lst, item):
    """ efficient `item in lst` for sorted lists """
    # if item is larger than the last its not in the list, but the bisect would 
    # find `len(lst)` as the index to insert, so check that first. Else, if the 
    # item is in the list then it has to be at index bisect_left(lst, item)
    return (item <= lst[-1]) and (lst[bisect_left(lst, item)] == item)

def extractUrlAndEmails(text):
    emails = re.findall(r'[\w\.-]+@[\w\.-]+', text)
    urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)
    return [ urls, emails ]

def split(txt, seps):
    default_sep = seps[0]
    # we skip seps[0] because that's the default seperator
    for sep in seps[1:]:
        txt = txt.replace(sep, default_sep)
    return [i.strip() for i in txt.split(default_sep)]

def sentencesExtract(text):
    return nltk.sent_tokenize( text )

def utf8lower(str1):
    try:
        res=str1.decode('utf-8').lower()
        #res=(str1.lower()).encode('utf8')
        #str1=((latinupper(str1)).lower()).encode('utf8').decode('utf8')
        return res
    except Exception, e:
        pass
    return str1.lower()

def utf8(str1):
    try:
        res=str1.decode('utf-8')
        return res
    except Exception, e:
        pass
    return str1

def latinupper(str1):
    try:
        res=str1.decode('latin-1').encode('latin-1').upper()
        return res
    except Exception, e:
        pass
    return str1

def latinlower(str1):
    try:
        res=str1.decode('latin-1').encode('latin-1').lower()
        return res
    except Exception, e:
        pass
    return str1

def chunks(l, n):
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def cleanCode(code):
    outCode = ''
    for c in code:
        if c.lower() in ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'z', 'y', 'w', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '~', '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '{', '}', '[', ']', "'", '"', ':', ';', '/', '?', '<', ',', '>', '.', '\\', '|', '-', '_', '=', '+', ' ', '\n'):
            outCode += c
    return outCode

def decodeUtf8(string):
    return string.decode('latin-1').encode('utf8')

def getPageWithCookies(urlPagina):
    try:
        jar = cookielib.FileCookieJar("cookies")
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(jar)) 
        opener.addheaders = [ ('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11') ]
        pag = opener.open(urlPagina)
        return pag
    except Exception, e3:
        return None  

def getPage(urlPagina):
    try:
        pag = urllib2.urlopen(urlPagina,timeout=1000)
        return pag
    except Exception, e:
        headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
            'Accept-Encoding': 'none',
            'Accept-Language': 'en-US,en;q=0.8',
            'Connection': 'keep-alive'}
        request = urllib2.Request(urlPagina, '', headers)
        try:
            pag = urllib2.urlopen(request,timeout=1000)
            return pag
        except Exception, e2:
            return getPageWithCookies(urlPagina)
        
def trim(vl):
    str=""
    lett_init=False
    for i in vl:
        if i != ' ' or lett_init:
            str+=i
            lett_init=True
    i=len(str)
    lett_init=False
    str2=""
    i=len(str)-1
    while i >=0 :
        if str[i] != ' ' or lett_init:
            str2=str[i]+str2
            lett_init=True
        i-=1
    return str2

def s_sanity(st):
    r=''
    ant=''
    for i in st:
        if i == ' ' and ant == ' ':
            continue
        else:
            r+=i
        ant=i
    return r

def readJsonFile(filepath):
    destino = None
    with open(filepath) as data_file:
        destino = json.load(data_file)
    return destino    
    
def validaExpr(mapa, expressao):
    #mapa={ "a":eval('True'), "b":eval('False'), "c":eval('False') }
    for key, val in mapa.iteritems():
        exec(key+'='+str(val))
    result = eval(expressao) #'(not c & (a ^ (b ^ c)))'
    for key, val in mapa.iteritems():
        exec('del '+key)
    return result

def readFileCode(__layouts__, categoria, arquivo):
    f = open(__layouts__ + 'codes/' + categoria + '/' + arquivo, "r")
    return f.read()

def debug(var):
    print var

def showNodes(parent):
    print "Sentence:", " ".join(parent.leaves())
    for node in parent:
        if type(node) is nltk.Tree:
            print "Nodo:", node.label()
            print "Childs:", node.leaves()
            showNodes(node)
        else:
            print "Word:", node