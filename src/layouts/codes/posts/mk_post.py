#mk_post.py

def run(dispach, logger, procName, mensagem=None, retorno=None):
    logger.info("EXEC ["+procName+"] retorno["+str(retorno)+"]")
    db = MongoClient().ghost
    if retorno!=None and 'produto' in retorno:
        if retorno!=None and 'post' in retorno:
            prod = db['collect_product'].find_one({"_id": retorno['produto']})
            if prod!=None:
                ch = db['channels'].find_one({"username": "ghost"})
                post = db.posts.find_one({"_id": ObjectId(str(retorno['post']))})
                if post!=None:
                    user = post['iduser_src']
                    newPost = {
                        "idextern": "null",
                        "extractRes": True,
                        "idchannel": str(ch['_id']),
                        "likes": "null",
                        "created_time": datetime.now(),
                        "procSema": True,
                        "iduser_src": "null",
                        "message": mensagem+"\n"+prod['titulo']+"\n Link: "+prod['_id'], #colocar o link e o valor
                        "resources": [{"source": prod['imagem'], "type": "photo", "text": ""}],
                    }
                    idNewPost = db.posts.save(newPost)
                    db.to_posting.save({
                        "iduser": user,
                        "idpost": str(idNewPost),
                        "iduser_src": "null",
                        "nameuser_src": "null",
                        "created_time": datetime.now(),
                        "idchannel": str(ch['_id']),
                        "synchronized": 0
                    })
                    logger.info("Postagem gerada para o usuario["+user+"] -> "+str(idNewPost))
            else:
                logger.error("Produto nao encontrado: "+str(retorno['produto']))
        else:
            logger.error("Post para passado ao job esta vazio")
    else:
        logger.error("Produto para passado ao job esta vazio")
    dispach(procName, False, True)
