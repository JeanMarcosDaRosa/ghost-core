import nltk
from nltk import CFG, grammar, parse, word_tokenize

sentencas = [
    '0 0 1 0 1 0 1 1 0', 		#correto
    '1 1 0 1', 					#invalido
    '0 1 1 1 0 1 0 0 1 1 1 0', 	#correto
    '1 0 1 0 0 1 0 1 0 1 1 0', 	#correto
]

g="""
S -> SC | SE
SE -> E CE | E C | E SE
SC -> C | C SC | C S
CE -> C E | CE C
C -> '0' '1' '1' | '1' '0' '1' | '1' '1' '0' | '0' '0' '0' 
E -> '0' '0' '1' | '1' '1' '1' | '0' '1' '0' | '1' '0' '0'

"""

print g
grammar = CFG.fromstring(g);
print "INICIO================================================"
parser = nltk.ChartParser(grammar)
for sent in sentencas:
	tokens = nltk.word_tokenize(sent)
	print tokens
	try:
		for tree in parser.parse(tokens):
			print(tree)
			break
	except:
		print "Sentenca invalida!"
print "FIM==================================================="
