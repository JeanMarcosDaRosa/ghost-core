#submarino.py

db = MongoClient().ghost

def parse_metas(soup):
  # parse html itens
  descr_P=''.encode('latin-1','ignore')
  descr_All=soup.findAll("h1",attrs={'class':'mp-tit-name'})
  for ds in descr_All:
    for d in ds.contents:
      tx=''
      if type(d).__name__ == 'Tag':
        tx= d.getText(' ').encode('latin-1', 'ignore')
      if type(d).__name__ == 'NavigableString':
        tx=d.encode('latin-1', 'ignore')
      descr_P+=' '+tx

  descr_P=su.s_sanity(descr_P)
  descr_P=descr_P.replace('\t', '').replace('\n', '')
  rt=[]
  rt.append(['descricao',descr_P])

  # preco
  descr_P=''.encode('latin-1','ignore')
  descr_All=soup.findAll("span",attrs={'itemprop':'price'})
  rfir=True
  for ds in descr_All:
    descr_P=''
    for d in ds.contents:
      tx=''
      if type(d).__name__ == 'Tag':
        tx= d.getText(' ').encode('latin-1', 'ignore')
      if type(d).__name__ == 'NavigableString':
        tx=d.encode('latin-1', 'ignore')
      descr_P+=' '+tx
    descr_P=su.s_sanity(descr_P)
    rt.append([u'Preço',descr_P])
    rfir=False

  # fotos
  descr_P=''.encode('latin-1','ignore')
  descr_All=soup.findAll("img",attrs={'class':'ip-photo active'})
  rfir=True
  for ds in descr_All:
    descr_P=''
    try:
      if ds.has_key('src'):
        tx=ds['src']
        rt.append(['##imagem',tx])
    except:
      pass
    rfir=False
  # parse categoria
  categ=soup.findAll("span",attrs={'class':'span-bc'})
  caracts_cateh=[]
  cats=[]
  atct=False
  for ittb in  categ:
    lnc=len(ittb.contents)
    for sp in ittb.contents:
      if getattr(sp, 'name', None) == 'span':
        for elmZ in sp.contents:      
          if getattr(elmZ, 'name', None) == 'span':
            for elm in elmZ.contents:
              if getattr(elm, 'name', None) == 'a':
                tx=''
                if type(elm).__name__ == 'Tag':
                  tx=elm.getText(' ').encode('latin-1', 'ignore')
                if type(elm).__name__ == 'NavigableString':
                  tx=elm.encode('latin-1', 'ignore')
                tx=tx.replace(' - ','\n')
                tx=su.decodeUtf8(su.trim(tx))
                caracts_cateh.append(tx)
  # parse caracts table
  rt2=[]
  rt2.append(caracts_cateh)
  matrizcG=[]
  matrizc=[]
  section=soup.findAll("section",attrs={'class':'ficha-tecnica'} )
  for sec in section:
    for tbl_ln in sec:
      if getattr(tbl_ln, 'name', None) == 'table':
        for elm in tbl_ln.contents:
          if getattr(elm, 'name', None) == 'tr':
            tx1=''
            tx2=''
            try:
              ak=elm.contents
            except:
              continue
            for elm2 in elm.contents:
              if getattr(elm2, 'name', None) == 'th':
                for dt1 in elm2.contents:
                  tx=''
                  if type(dt1).__name__ == 'Tag':
                    tx=dt1.getText(' ').encode('latin-1', 'ignore')
                  if type(dt1).__name__ == 'NavigableString':
                    tx=dt1.encode('latin-1', 'ignore')
                  tx=tx.replace(' - ','\n')
                  tx=su.trim(tx)
                  if len(tx)>0:
                    tx = tx.replace('\r', ' ').replace('\n', ' ')
                  tx1+=su.s_sanity(tx)
              if getattr(elm2, 'name', None) == 'td':
                for dt2 in elm2.contents:
                  tx=''
                  if type(dt2).__name__ == 'Tag':
                    tx=dt2.getText(' ').encode('latin-1', 'ignore')
                  if type(dt2).__name__ == 'NavigableString':
                    tx=dt2.encode('latin-1', 'ignore')
                  tx=tx.replace(' - ','\n')
                  tx=su.trim(tx)
                  if len(tx)>0:
                    tx = tx.replace('\r', ' ').replace('\n', ' ')
                  tx2+= su.s_sanity(tx)
            if tx1 != '' and tx2 != '' :
              matrizc.append([tx1,tx2])
        #matrizcG2[0] -> caracts gerais, matrizcG[1] -> especif tecnicas
        matrizcG.append(matrizc)
  rt2.append(rt)
  rt2.append(matrizcG)
  return rt2

matriz_prod=[]

def parse_page(soup,orig_url):
  global start, matriz_prod
  metas=parse_metas(soup)
  cats=[]
  infos=[]
  tab_Caracts=[]
  tab_tecnica=[]
  preco=''
  cats=metas[0]
  infos=metas[1]
  try:
    tab_Caracts=metas[2][0]
  except Exception,e:
    tab_Caracts=[]
    print 'Error:', e, 'url:', orig_url

  # apenas o ultimo item == descricao. os demais serao adicionados na tab diretamente
  t_title=''
  t_image=''
  info_prod={}
  if len(infos) > 0:
    cnd=0
    for i in infos:
      title=su.trim(su.decodeUtf8((i[0]).replace('.','')))
      value=su.trim(su.decodeUtf8(i[1]))
      if title == '##imagem':
        t_image=i[1]
        continue
      if title == 'descricao':
        t_title=value
      else:
        info_prod[title]=value
      cnd+=1
  #==========
  if len(tab_Caracts) > 0 :
    for i in tab_Caracts:
      title=su.trim(su.decodeUtf8((i[0]).replace('.','')))
      value=su.trim(su.decodeUtf8(i[1]))
      if title == 'descricao':
        t_title=value
      else:
        info_prod[title]=value
  #==========
  if len(tab_tecnica) > 0 :
    for i in tab_tecnica:
      title=su.trim(su.decodeUtf8((i[0]).replace('.','')))
      value=su.trim(su.decodeUtf8(i[1]))
      info_prod[title]=value
  #separar ao maximo as definicoes / categorias / preco /
  return {'_id':orig_url, 'categorias': cats,'definicoes':info_prod, 'titulo': t_title, 'imagem':t_image, 'downimg': False}

#remover &quote e ajustar a "ç" no "Preco" 
def run(dispach, logger, procName, root, quantidade=1, reprocessa=False, retorno=None):
  logger.info("EXEC ["+procName+"]: "+str(root)+" quantidade: "+str(quantidade))
  try:
    for p in db.collect_links_product.find({"root": root, "indexed": False, "inproc": False}).limit(quantidade):
      link = p['_id']
      db.collect_links_product.update({"_id": link},{"$set": {"inproc": True}})
      if reprocessa or db.collect_product.find_one({"_id": su.trim(link)})==None:
        pagina = su.getPage(link)
        if pagina!=None:
          logger.info('URL: '+link+' - OK')
          soup = BeautifulSoup(pagina.read().decode('utf-8', 'ignore'))
          prod = parse_page(soup,link)
          #logger.info(str(prod))
          if db.collect_product.find_one({"_id": su.trim(link)})==None:
            logger.debug("Inseriu produto: ["+link+"]")
            db.collect_product.insert(prod)
          else:
            logger.debug("Atualizou produto: ["+link+"]")
            db.collect_product.update({"_id":link}, prod)
          #dispach(procName,{'produtos': prod['imagem']}, True)
          db.collect_links_product.update({"_id": link},{"$set": {"indexed": True}})
    dispach(procName,False, True)
  except Exception,e:
    logger.error(e)
    dispach(procName,False, True)
